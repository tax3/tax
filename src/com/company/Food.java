package com.company;

public class Food
{
    private final double price;
    private final double tax = 0.08;

    public Food(double price)
    {
        this.price = price;
    }

    public double GetPrice()
    {
        return price + (price * tax);
    }
}
